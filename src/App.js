import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import { members } from "./components/members/members";
import Customer from "./pages/Customer";
import Company from "./pages/Company";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home members={members} />
        </Route>

        <Route exact path="/customer/:id">
          <Customer />
        </Route>

        <Route exact path="/company/:id">
          <Company />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
