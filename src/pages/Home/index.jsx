import { Link } from "react-router-dom";

import "./style.css";

const Home = ({ members }) => {
  return (
    <ul>
      {members.map((item, index) => (
        <li key={index}>
          <Link
            to={
              item.type === "pj"
                ? `/company/${item.id}`
                : `/customer/${item.id}`
            }
          >
            {item.name}
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default Home;
